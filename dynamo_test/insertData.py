import boto3
import os
import json
import time
from random import randint, choice, uniform
from botocore.exceptions import ClientError

session = boto3.Session(
    aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
    region_name=os.environ['AWS_REGION']
)

dynamodb = session.resource('dynamodb')
table_name = os.environ['TABLE_NAME']
table = dynamodb.Table(table_name)

def generate_data(i):
    movie_id = f"MOV{i:03d}"
    book_id = f"BOOK{i+100:03d}"
    restaurant_id = f"REST{i+200:03d}"
    
    genres = ["Action", "Comedy", "Drama", "Sci-Fi", "Horror", "Romance"]
    ratings = ["G", "PG", "PG-13", "R", "NC-17"]
    cuisines = ["American", "Italian", "Chinese", "Mexican", "Indian", "Japanese"]
    
    return {
        'MovieID': {'S': movie_id},
        'Title': {'S': f"The Sample Movie {i}"},
        'Year': {'N': str(randint(2000, 2024))},
        'Genre': {'S': choice(genres)},
        'Rating': {'S': choice(ratings)},
        'BookID': {'S': book_id},
        'modified': {'N': str(int(time.time() * 1000))},
        'Author': {'S': f"Author {i}"},
        'PublishYear': {'S': str(randint(1900, 2024))},
        'InPrint': {'N': str(randint(0, 1))},
        'RestaurantID': {'S': restaurant_id},
        'Name': {'S': f"Cinema Cafe {i}"},
        'Cuisine': {'S': choice(cuisines)},
        'Delivery': {'N': str(randint(0, 1))}
    }

def write_to_dynamodb(item):
    try:
        response = table.put_item(Item=item)
        print(f"Successfully inserted item with MovieID: {item['MovieID']}")
        return response
    except ClientError as e:
        print(f"Error writing to DynamoDB: {e.response['Error']['Message']}")
        return None

for i in range(2):  
    data = generate_data(i)
    print(json.dumps(data, indent=2))
    
    dynamodb_item = {}
    for k, v in data.items():
        if 'S' in v:
            dynamodb_item[k] = v['S']
        elif 'N' in v:
            dynamodb_item[k] = int(v['N'])
    
    write_to_dynamodb(dynamodb_item)