import boto3
import os
from random import randint, choice, uniform
from decimal import Decimal
from botocore.exceptions import ClientError

# Initialize a session using your credentials
session = boto3.Session(
    aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
    region_name=os.environ['AWS_REGION']
)

# Initialize DynamoDB client
dynamodb = session.resource('dynamodb')
table_name = os.environ['TABLE_NAME']
table = dynamodb.Table(table_name)

def delete_all_items():
   # Scan the table to get all items
   scan = table.scan()
   with table.batch_writer() as batch:
       for each in scan['Items']:
           batch.delete_item(
               Key={
                   'MovieID': each['MovieID']  # Replace with your table's primary key
               }
           )
   print(f"Deleted {len(scan['Items'])} items from {table_name}")

# Main execution
if __name__ == '__main__':
   delete_all_items()