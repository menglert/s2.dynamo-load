import json
import pymysql
import os
import re

def connect_singlestore():
    return pymysql.connect(
        host=os.environ['SINGLESTORE_HOST'],
        user=os.environ['SINGLESTORE_USER'],
        password=os.environ['SINGLESTORE_PASSWORD'],
        db=os.environ['SINGLESTORE_DB'],
        cursorclass=pymysql.cursors.DictCursor
    )

def extract_table_name(arn):
    match = re.search(r'table/([^/]+)', arn)
    if match:
        return match.group(1)
    return None

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def lambda_handler(event, context):
    if not event or (isinstance(event, dict) and not event):
        print("Received empty event. Exiting.")
        return {
            'statusCode': 200,
            'body': json.dumps('Empty event received, no action taken')
        }

    table_prefix = os.environ.get('TABLE_PREFIX', '')
    
    for record in event['Records']:
        print(record)
        if record['eventSource'] == 'aws:dynamodb':
            connection = connect_singlestore()

            event_name = record['eventName']
            full_table_name = extract_table_name(record['eventSourceARN'])
            if not full_table_name:
                print("Could not extract table name from ARN")
                continue
            
            table_name = remove_prefix(full_table_name, table_prefix)

            if event_name == 'INSERT':
                try:
                    with connection.cursor() as cursor:
                        json_str = json.dumps(record)
                        sql = """CALL p_{}_single('{}')""".format(table_name.lower(), json_str)
                        cursor.execute(sql)
                        connection.commit()
                except Exception as e:
                    print(f"Error: {str(e)}")
                finally:
                    connection.close()
            elif event_name == 'REMOVE':
                try:
                    with connection.cursor() as cursor:
                        json_str = json.dumps(record)
                        sql = """CALL p_{}_delete('{}')""".format(table_name.lower(), json_str)
                        cursor.execute(sql)
                        connection.commit()
                except Exception as e:
                    print(f"Error: {str(e)}")
                finally:
                    connection.close()
            elif event_name == 'MODIFY':
                try:
                    with connection.cursor() as cursor:
                        json_str = json.dumps(record)
                        sql = """CALL p_{}_modify('{}')""".format(table_name.lower(), json_str)
                        print(sql)
                        cursor.execute(sql)
                        connection.commit()
                except Exception as e:
                    print(f"Error: {str(e)}")
                finally:
                    connection.close()
            else:
                continue 
        else:
            continue
    return {
        'statusCode': 200,
        'body': json.dumps('Data processed successfully')
    }