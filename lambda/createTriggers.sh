#!/bin/bash

# Default values
WHITELIST=""
BLACKLIST=""
LAMBDA_ARN=""
BATCH_SIZE=100
ENABLED_TABLES=""

# Function to print usage
usage() {
    echo "Usage: $0 [-w WHITELIST] [-b BLACKLIST] [-l LAMBDA_ARN] [-s BATCH_SIZE]"
    echo "  -w WHITELIST     Comma-separated list of table names to include"
    echo "  -b BLACKLIST     Comma-separated list of table names to exclude"
    echo "  -l LAMBDA_ARN    ARN of the Lambda function to trigger"
    echo "  -s BATCH_SIZE    Batch size for the trigger (default: 100)"
    exit 1
}

# Parse command line options
while getopts ":w:b:l:s:" opt; do
    case $opt in
        w) WHITELIST=$OPTARG ;;
        b) BLACKLIST=$OPTARG ;;
        l) LAMBDA_ARN=$OPTARG ;;
        s) BATCH_SIZE=$OPTARG ;;
        *) usage ;;
    esac
done

# Check if Lambda ARN is provided
if [ -z "$LAMBDA_ARN" ]; then
    echo "Error: Lambda ARN is required"
    usage
fi

# Get all table names if no whitelist is provided
if [ -z "$WHITELIST" ]; then
    TABLES=$(aws dynamodb list-tables --query 'TableNames[]' --output text)
else
    IFS=',' read -ra WHITELIST_ARRAY <<< "$WHITELIST"
    TABLES="${WHITELIST_ARRAY[@]}"
fi

# Convert blacklist to array
IFS=',' read -ra BLACKLIST_ARRAY <<< "$BLACKLIST"

# Iterate over tables
for TABLE in $TABLES; do
    # Check if table is in blacklist
    if [[ " ${BLACKLIST_ARRAY[@]} " =~ " ${TABLE} " ]]; then
        echo "Skipping blacklisted table: $TABLE"
        continue
    fi

    echo "Processing table: $TABLE"

    # Check if stream is already enabled
    STREAM_ENABLED=$(aws dynamodb describe-table --table-name "$TABLE" --query 'Table.StreamSpecification.StreamEnabled' --output text)

    if [ "$STREAM_ENABLED" = "True" ]; then
        echo "Stream is already enabled for table: $TABLE"
    else
        echo "Enabling stream for table: $TABLE"
        aws dynamodb update-table \
            --table-name "$TABLE" \
            --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES

        if [ $? -ne 0 ]; then
            echo "Failed to enable stream for table: $TABLE"
            continue
        fi
    fi

    # Get the latest stream ARN
    STREAM_ARN=$(aws dynamodb describe-table --table-name "$TABLE" --query 'Table.LatestStreamArn' --output text)

    # Check if trigger already exists
    EXISTING_TRIGGER=$(aws lambda list-event-source-mappings --function-name "$LAMBDA_ARN" --event-source-arn "$STREAM_ARN" --query 'EventSourceMappings[0].UUID' --output text)

    if [ "$EXISTING_TRIGGER" != "None" ]; then
        echo "Trigger already exists for table: $TABLE"
    else
        echo "Creating trigger for table: $TABLE"
        aws lambda create-event-source-mapping \
            --function-name "$LAMBDA_ARN" \
            --event-source "$STREAM_ARN" \
            --batch-size "$BATCH_SIZE" \
            --starting-position LATEST

        # Check if trigger was successfully created
        if [ $? -eq 0 ]; then
            echo "Trigger created successfully for table: $TABLE"
            ENABLED_TABLES+="$TABLE,"
        else
            echo "Failed to create trigger for table: $TABLE"
        fi
    fi
done

# Remove trailing comma from ENABLED_TABLES
ENABLED_TABLES=${ENABLED_TABLES%,}

echo "Triggers have been created for the following tables: $ENABLED_TABLES"