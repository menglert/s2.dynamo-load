#!/bin/bash

mkdir pymysql-layer
cd pymysql-layer
python3 -m venv venv
source venv/bin/activate
pip3 install pymysql -t .
mkdir -p python/lib/python3.12/site-packages
cp -r pymysql python/lib/python3.12/site-packages
zip -r pymysql-layer.zip python

exit 0