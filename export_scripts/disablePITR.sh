#!/bin/bash

# Default values
WHITELIST=""
BLACKLIST=""
DISABLED_TABLES=""

# Function to print usage
usage() {
    echo "Usage: $0 [-w WHITELIST] [-b BLACKLIST]"
    echo "  -w WHITELIST     Comma-separated list of table names to include"
    echo "  -b BLACKLIST     Comma-separated list of table names to exclude"
    exit 1
}

# Parse command line options
while getopts ":w:b:" opt; do
    case $opt in
        w) WHITELIST=$OPTARG ;;
        b) BLACKLIST=$OPTARG ;;
        *) usage ;;
    esac
done

# Get all table names if no whitelist is provided
if [ -z "$WHITELIST" ]; then
    TABLES=$(aws dynamodb list-tables --query 'TableNames[]' --output text)
else
    IFS=',' read -ra WHITELIST_ARRAY <<< "$WHITELIST"
    TABLES="${WHITELIST_ARRAY[@]}"
fi

# Convert blacklist to array
IFS=',' read -ra BLACKLIST_ARRAY <<< "$BLACKLIST"

# Iterate over tables
for TABLE in $TABLES; do
    # Check if table is in blacklist
    if [[ " ${BLACKLIST_ARRAY[@]} " =~ " ${TABLE} " ]]; then
        echo "Skipping blacklisted table: $TABLE"
        continue
    fi

    echo "Processing table: $TABLE"

    # Check PITR status
    PITR_STATUS=$(aws dynamodb describe-continuous-backups --table-name "$TABLE" --query 'ContinuousBackupsDescription.PointInTimeRecoveryDescription.PointInTimeRecoveryStatus' --output text)

    if [ "$PITR_STATUS" = "DISABLED" ]; then
        echo "PITR is already disabled for table: $TABLE"
    else
        echo "Disabling PITR for table: $TABLE"
        aws dynamodb update-continuous-backups \
            --table-name "$TABLE" \
            --point-in-time-recovery-specification PointInTimeRecoveryEnabled=false

        # Check if PITR was successfully disabled
        if [ $? -eq 0 ]; then
            echo "PITR disabled successfully for table: $TABLE"
            DISABLED_TABLES+="$TABLE,"
        else
            echo "Failed to disable PITR for table: $TABLE"
        fi
    fi
done

# Remove trailing comma from DISABLED_TABLES
DISABLED_TABLES=${DISABLED_TABLES%,}

echo "PITR has been disabled for the following tables: $DISABLED_TABLES"