#!/bin/bash

# Default values
WHITELIST=""
BLACKLIST=""
TARGET_FOLDER="./table-info"
PREFIX=""

# Function to print usage
usage() {
    echo "Usage: $0 [-w WHITELIST] [-b BLACKLIST] [-t TARGET_FOLDER] [-p PREFIX]"
    echo "  -w WHITELIST     Comma-separated list of table names to include"
    echo "  -b BLACKLIST     Comma-separated list of table names to exclude"
    echo "  -t TARGET_FOLDER Target folder for output files (default: ./output)"
    echo "  -p PREFIX        Prefix to remove from table names"
    exit 1
}

# Parse command line options
while getopts ":w:b:t:p:" opt; do
    case $opt in
        w) WHITELIST=$OPTARG ;;
        b) BLACKLIST=$OPTARG ;;
        t) TARGET_FOLDER=$OPTARG ;;
        p) PREFIX=$OPTARG ;;
        *) usage ;;
    esac
done

# Create target folder if it doesn't exist
mkdir -p "$TARGET_FOLDER"

# Get all table names if no whitelist is provided
if [ -z "$WHITELIST" ]; then
    TABLES=$(aws dynamodb list-tables --query 'TableNames[]' --output text)
else
    IFS=',' read -ra WHITELIST_ARRAY <<< "$WHITELIST"
    TABLES="${WHITELIST_ARRAY[@]}"
fi

# Convert blacklist to array
IFS=',' read -ra BLACKLIST_ARRAY <<< "$BLACKLIST"

# Iterate over tables
for TABLE in $TABLES; do
    # Check if table is in blacklist
    if [[ " ${BLACKLIST_ARRAY[@]} " =~ " ${TABLE} " ]]; then
        continue
    fi

    # Remove prefix from table name for output file
    OUTPUT_NAME=${TABLE#"$PREFIX"}

    echo "Processing table: $TABLE"
    aws dynamodb describe-table --table-name "$TABLE" > "$TARGET_FOLDER/${OUTPUT_NAME}.json"
done

echo "Table descriptions have been saved to $TARGET_FOLDER"