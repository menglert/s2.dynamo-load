#!/bin/bash

# Default values
WHITELIST=""
BLACKLIST=""
S3_BUCKET=""
PREFIX=""

# Function to print usage
usage() {
    echo "Usage: $0 -s S3_BUCKET [-w WHITELIST] [-b BLACKLIST] [-p PREFIX]"
    echo "  -s S3_BUCKET     S3 bucket to store the exports"
    echo "  -w WHITELIST     Comma-separated list of table names to include"
    echo "  -b BLACKLIST     Comma-separated list of table names to exclude"
    echo "  -p PREFIX        Prefix to remove from table names"
    exit 1
}

# Parse command line options
while getopts ":w:b:s:p:" opt; do
    case $opt in
        w) WHITELIST=$OPTARG ;;
        b) BLACKLIST=$OPTARG ;;
        s) S3_BUCKET=$OPTARG ;;
        p) PREFIX=$OPTARG ;;
        *) usage ;;
    esac
done

# Check if S3 bucket is provided
if [ -z "$S3_BUCKET" ]; then
    echo "Error: S3 bucket is required"
    usage
fi

# Get all table names if no whitelist is provided
if [ -z "$WHITELIST" ]; then
    TABLES=$(aws dynamodb list-tables --query 'TableNames[]' --output text)
else
    IFS=',' read -ra WHITELIST_ARRAY <<< "$WHITELIST"
    TABLES="${WHITELIST_ARRAY[@]}"
fi

# Convert blacklist to array
IFS=',' read -ra BLACKLIST_ARRAY <<< "$BLACKLIST"

# Iterate over tables
for TABLE in $TABLES; do
    # Check if table is in blacklist
    if [[ " ${BLACKLIST_ARRAY[@]} " =~ " ${TABLE} " ]]; then
        continue
    fi

    # Remove prefix from table name for S3 folder
    S3_FOLDER=dynamo-export/data/${TABLE#"$PREFIX"}

    echo "Exporting table: $TABLE"

    # Create an export task
    EXPORT_ARN=$(aws dynamodb export-table-to-point-in-time \
        --table-arn $(aws dynamodb describe-table --table-name "$TABLE" --query 'Table.TableArn' --output text) \
        --s3-bucket "$S3_BUCKET" \
        --s3-prefix "$S3_FOLDER/" \
        --export-format DYNAMODB_JSON \
        --query 'ExportDescription.ExportArn' \
        --output text)

    echo "Export started for $TABLE. Export ARN: $EXPORT_ARN"
done

echo "All table exports have been initiated."