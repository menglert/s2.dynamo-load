#!/bin/bash

# Default values
SOURCE="table-info"
TARGET=""

# Function to print usage
usage() {
    echo "Usage: $0 [-s SOURCE] [-t TARGET] [-l LAMBDA_ARN] [-s BATCH_SIZE]"
    echo "  -s SOURCE     Comma-separated list of table names to include"
    echo "  -t TARGET     Comma-separated list of table names to exclude"
    exit 1
}

# Parse command line options
while getopts ":s:t:" opt; do
    case $opt in
        s) SOURCE=$OPTARG ;;
        t) TARGET=$OPTARG ;;
        *) usage ;;
    esac
done

aws s3 cp ./$SOURCE/ s3://$TARGET/$SOURCE/ --recursive